//
//  YCBlocks.h
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

typedef void (^void_block_string)(NSString*);
typedef void (^void_block_id)(id);
typedef void (^void_block_array)(NSArray*);
typedef void (^void_block_bool_arr)(BOOL success, NSArray *errors);
typedef void (^void_block_void)(void);
