//
//  Translation.h
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Word;

@interface Translation : NSManagedObject

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) Word *word;

@end
