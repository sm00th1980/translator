//
//  Translation.m
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "Translation.h"
#import "Word.h"


@implementation Translation

@dynamic text;
@dynamic word;

@end
