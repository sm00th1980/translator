//
//  Word.m
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "Word.h"
#import "Translation.h"
#import "YCTranslateEngine.h"

@implementation Word

@dynamic text;
@dynamic loadingTranslation;
@dynamic translations;

+ (NSArray*)all
{
    return [self MR_findAllSortedBy:@"text" ascending:YES];
}

+ (void)createNewWordWithText:(NSString*)text
{
    Word *new_word = [self MR_createEntity];
    new_word.text = text;
    new_word.loadingTranslation = @YES;
    
    //loading translation
    [[YCTranslateEngine new] translateByText:text success:^(NSArray *translations) {
        new_word.loadingTranslation = @NO;
        [new_word addTranslations:translations];
        
        NSManagedObjectContext* context = new_word.managedObjectContext;
        [context MR_saveOnlySelfAndWait];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTranslationFinished object:nil];
    } failure:^(NSString *error) {
        new_word.loadingTranslation = @NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTranslationFinished object:nil];
    }];
}

- (void)drop
{
    NSManagedObjectContext* context = self.managedObjectContext;
    [self MR_deleteEntity];
    [context MR_saveOnlySelfAndWait];
}

- (BOOL)isLoadingTranslation
{
    return [self.loadingTranslation boolValue];
}

- (void)addTranslations:(NSArray*)translations
{
    for (NSString *text in translations) {
        Translation *translation = [Translation MR_createEntity];
        translation.text = text;
        translation.word = self;
    }
}

- (NSOrderedSet*)textTranslations
{
    return [self.translations bk_map:^id(id translation) {
        return [(Translation*)translation text];
    }];
}

+ (NSArray*)searchByTextOrTranslation:(NSString*)text
{
    if (text && ![text isEqualToString:@""]) {
        //something typed in search string -> try to searching
        
        //search words by text or any translations
        NSPredicate *wordsByTextFilter = [NSPredicate predicateWithFormat:@"(text CONTAINS[cd] %@) OR (ANY translations.text CONTAINS[cd] %@)", text, text];
        return [self MR_findAllWithPredicate:wordsByTextFilter];
    }
    
    //searh string is empty -> find all records
    return [self all];
}

+ (BOOL)isWordAlreadyExistWithText:(NSString*)text
{
    NSPredicate *wordsByTextFilter = [NSPredicate predicateWithFormat:@"text == %@", text];
    NSArray *words = [self MR_findAllWithPredicate:wordsByTextFilter];
    
    if (words.count == 0) {
        return NO;
    }
    
    return YES;
}

@end
