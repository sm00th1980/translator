//
//  Word.h
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Translation;

@interface Word : NSManagedObject

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSNumber * loadingTranslation;
@property (nonatomic, retain) NSOrderedSet *translations;

+ (NSArray*)all;
+ (void)createNewWordWithText:(NSString*)text;
- (BOOL)isLoadingTranslation;
- (NSOrderedSet*)textTranslations;
+ (NSArray*)searchByTextOrTranslation:(NSString*)text;
+ (BOOL)isWordAlreadyExistWithText:(NSString*)text;
- (void)drop;

@end
