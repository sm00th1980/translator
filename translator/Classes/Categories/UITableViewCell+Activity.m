//
//  UITableViewCell+Activity.m
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "UITableViewCell+Activity.h"
#import <objc/runtime.h>

static const char *kActivityIndicatorKey =  "kActivityIndicatorKey";

@implementation UITableViewCell (Activity)

- (UIActivityIndicatorView*)utils_createActivityIndicator
{
    UIActivityIndicatorView* ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    ai.color = [UIColor darkGrayColor];
    ai.hidesWhenStopped = YES;
    
    return ai;
}

- (UIActivityIndicatorView*)utils_activityIndicator
{
    UIActivityIndicatorView *ai = objc_getAssociatedObject(self, (void*)kActivityIndicatorKey);
    if(!ai) {
        ai = [self utils_createActivityIndicator];
        [self.contentView addSubview:ai];
        objc_setAssociatedObject(self, kActivityIndicatorKey, ai, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return ai;
}


- (void)execInMainThread:(void (^)())blk
{
    if([NSThread isMainThread]) {
        if(blk)blk();
    } else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            if(blk)blk();
        });
    }
}


#pragma mark public

- (void)showActivity
{
    [self execInMainThread:^{
        self.contentView.userInteractionEnabled = NO;
        
        UIActivityIndicatorView* ai = [self utils_activityIndicator];
        ai.center = CGPointMake(self.contentView.frame.size.width - ai.frame.size.width,
                                self.contentView.center.y);
        [ai startAnimating];
    }];
}

- (void)hideActivity
{
    [self execInMainThread:^{
        self.contentView.userInteractionEnabled = YES;
        UIActivityIndicatorView* ai = [self utils_activityIndicator];
        [ai stopAnimating];
    }];
}

@end
