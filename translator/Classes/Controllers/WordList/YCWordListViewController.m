//
//  WordListViewController.m
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

static CGFloat const DEFAULT_CELL_HEIGHT = 50.0f;
static NSString * const kYCWordCellReuseIdentifier = @"kYCWordCellReuseIdentifier";

#import "YCWordListViewController.h"
#import "Word.h"
#import "UITableViewCell+Activity.h"
#import "JAActionButton.h"
#import "YCWordCell.h"

@interface YCWordListViewController () <UISearchResultsUpdating, JASwipeCellDelegate>
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSArray *words;
@end

@implementation YCWordListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[YCWordCell class] forCellReuseIdentifier:kYCWordCellReuseIdentifier];
    
    [self showSearchBar];
    [self setupNotifications];
    
    [self loadAllWords];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadAllWords) name:kNotificationTranslationFinished object:nil];
}

- (NSArray *)leftButtons
{
    __typeof(self) __weak weakSelf = self;
    JAActionButton *deleteButton = [JAActionButton actionButtonWithTitle:NSLocalizedString(@"Delete", nil) color:[UIColor redColor] handler:^(UIButton *actionButton, JASwipeCell*cell) {
        [weakSelf leftMostButtonSwipeCompleted:cell];
    }];
    
    return @[deleteButton];
}

- (NSArray *)rightButtons
{
    __typeof(self) __weak weakSelf = self;
    JAActionButton *deleteButton = [JAActionButton actionButtonWithTitle:NSLocalizedString(@"Delete", nil) color:[UIColor redColor] handler:^(UIButton *actionButton, JASwipeCell*cell) {
        [weakSelf rightMostButtonSwipeCompleted:cell];
    }];
    
    return @[deleteButton];
}

- (void)loadAllWords
{
    self.words = [Word all];
}

- (void)setWords:(NSArray *)words
{
    self->_words = words;
    [self.tableView reloadData];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.words.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YCWordCell *cell = [tableView dequeueReusableCellWithIdentifier:kYCWordCellReuseIdentifier forIndexPath:indexPath];
    [cell addActionButtons:[self leftButtons] withButtonWidth:kJAButtonWidth withButtonPosition:JAButtonLocationLeft];
    [cell addActionButtons:[self rightButtons] withButtonWidth:kJAButtonWidth withButtonPosition:JAButtonLocationRight];
    
    // Configure the cell...
    Word *word = self.words[indexPath.row];
    
    if ([word isLoadingTranslation]) {
        [cell showActivity];
    } else {
        [cell hideActivity];
    }
    
    cell.delegate = self;
    
    [cell configureCellWithText:word.text translation:[[word textTranslations] firstObject]];
    [cell setNeedsLayout];
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    return cell;
}


- (void)showSearchBar
{
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    [self.searchController.searchBar sizeToFit]; //iOS8 bug => you have to set height
    
    self.searchController.searchResultsUpdater = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return DEFAULT_CELL_HEIGHT;
}

#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    self.words = [Word searchByTextOrTranslation:searchController.searchBar.text];
}

#pragma mark - JASwipeCellDelegate methods
- (void)leftMostButtonSwipeCompleted:(JASwipeCell *)cell
{
    [self dropCell:cell];
}

- (void)rightMostButtonSwipeCompleted:(JASwipeCell *)cell
{
    [self dropCell:cell];
}

- (void)dropCell:(JASwipeCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    Word *word = self.words[indexPath.row];
    [word drop];
    [self loadAllWords];
}
@end
