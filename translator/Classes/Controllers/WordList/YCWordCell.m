//
//  YCWordCell.m
//  translator
//
//  Created by Deyarov Ruslan on 16.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

static CGFloat const MARGIN = 15.0f;

#import "YCWordCell.h"
#import "PureLayout.h"

@interface YCWordCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *translateLabel;
@property (nonatomic) BOOL constraintsSetup;
@end

@implementation YCWordCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.topContentView addSubview:self.titleLabel];
        [self.topContentView addSubview:self.translateLabel];
    }
    return self;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [UILabel newAutoLayoutView];
        _titleLabel.numberOfLines = 0;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _translateLabel.textColor = [UIColor blackColor];

    }
    return _titleLabel;
}

- (UILabel *)translateLabel
{
    if (!_translateLabel) {
        _translateLabel = [UILabel newAutoLayoutView];
        _translateLabel.numberOfLines = 0;
        _translateLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _translateLabel.backgroundColor = [UIColor clearColor];
        _translateLabel.textColor = [UIColor grayColor];
    }
    return _translateLabel;
}

- (void)updateConstraints
{
    [super updateConstraints];
    
    if (!self.constraintsSetup) {

        [self.titleLabel autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:MARGIN];
        [self.titleLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        
        [self.translateLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:MARGIN];
        [self.translateLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        
        self.constraintsSetup = YES;
    }
}

- (void)configureCellWithText:(NSString *)text translation:(NSString*)translation
{
    self.titleLabel.text = text;
    self.translateLabel.text = translation;
}

@end
