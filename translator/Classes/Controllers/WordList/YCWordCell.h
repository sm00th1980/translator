//
//  YCWordCell.h
//  translator
//
//  Created by Deyarov Ruslan on 16.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "JASwipeCell.h"

@interface YCWordCell : JASwipeCell

- (void)configureCellWithText:(NSString *)text translation:(NSString*)translation;

@end
