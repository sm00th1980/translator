//
//  AddWordViewController.h
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BPFormViewController.h"

@interface YCAddWordViewController : BPFormViewController

@end
