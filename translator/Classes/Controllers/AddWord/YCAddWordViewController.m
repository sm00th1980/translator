//
//  AddWordViewController.m
//  translator
//
//  Created by Deyarov Ruslan on 15.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "YCAddWordViewController.h"
#import "BPFormInputTextFieldCell.h"
#import "BPFormButtonCell.h"
#import "BPAppearance.h"
#import "Word.h"
#import "AMSmoothAlertView.h"

#define ALERT_TITLE NSLocalizedString(@"Error", nil)
#define ALERT_TEXT NSLocalizedString(@"Such word is already exist in your dictionary", nil)

@interface YCAddWordViewController () <UITextFieldDelegate>
@property (nonatomic, strong) BPFormInputTextFieldCell *wordCell;
@end

@implementation YCAddWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [BPAppearance sharedInstance].inputCellTextFieldFont = [UIFont systemFontOfSize:35];
    self.customSectionHeaderHeight = 30;
    
    self.wordCell = [[BPFormInputTextFieldCell alloc] init];
    self.wordCell.textField.placeholder = NSLocalizedString(@"New word", nil);
    self.wordCell.textField.delegate = self;
    self.wordCell.customCellHeight = 50.0f;
    self.wordCell.mandatory = YES;
    
    self.formCells = @[@[self.wordCell]];
    
    [self setFooterTitle:NSLocalizedString(@"Input new word for translation", nil) forSection:0];
    
    [self start];
}

- (void)start
{
    self.wordCell.textField.text = @"";
    [self.wordCell.textField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    void (^closeBlock)(void) = ^() { [self dismissViewControllerAnimated:YES completion:nil]; };
    
    if ([Word isWordAlreadyExistWithText:textField.text]) {
        [self.wordCell.textField resignFirstResponder]; //close keyboard
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:ALERT_TITLE
                                                                             andText:ALERT_TEXT
                                                                     andCancelButton:NO
                                                                        forAlertType:AlertFailure
                                                               withCompletionHandler:^(AMSmoothAlertView *alert, UIButton *button) {
                                                                   closeBlock();
                                                               }];
        [alert show];
    } else {
        //creating new word
        [Word createNewWordWithText:textField.text];
        closeBlock();
    }
    
    return YES;
}

@end
