//
//  TranslateEngine.m
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

static NSString * const URL = @"https://translate.yandex.net/api/v1.5/tr.json/translate";
static NSString * const LANG = @"en-ru";
static NSString * const KEY = @"trnsl.1.1.20141112T120513Z.07ebf70b79e0f858.6e0386b8abc9d48b970169bcb0a55fe6a65ed722";

#import "YCTranslateEngine.h"

@interface YCTranslateEngine ()
@property (nonatomic, strong) NSString *text;
@end

@implementation YCTranslateEngine

- (void)translateByText:(NSString*)text
                success:(void_block_array)success
                failure:(void_block_string)failure
{
    //setup params
    self.text = text;
    
    void (^successCompletion)(NSDictionary *response) = ^(NSDictionary *response) {
        if ([response respondsToSelector:@selector(objectForKey:)] && response[@"text"]) {
            success(response[@"text"]);
        } else {
            failure(NSLocalizedString(@"Invalid server response", nil));
        }
    };
    
    [self fetch:URL success:successCompletion failure:failure];
}

- (NSDictionary*)params
{
    return @{
             @"key": KEY,
             @"text": self.text,
             @"lang": LANG
             };
}

@end
