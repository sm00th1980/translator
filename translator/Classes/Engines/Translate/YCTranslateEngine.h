//
//  TranslateEngine.h
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "YCBaseEngine.h"

@interface YCTranslateEngine : YCBaseEngine

- (void)translateByText:(NSString*)text
                success:(void_block_array)success
                failure:(void_block_string)failure;

@end
