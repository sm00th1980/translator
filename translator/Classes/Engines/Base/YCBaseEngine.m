//
//  YCBaseEngine.m
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

BOOL const DEBUG_NETWORK_REQUEST = NO;
BOOL const DEBUG_NETWORK_RESPONSE = NO;

#import "YCBaseEngine.h"
#import "AFHTTPRequestOperationManager.h"

@implementation YCBaseEngine

#pragma mark - Public Interface
-(void)fetch:(NSString*)URLString success:(void_block_id)success failure:(void_block_string)failure
{
    /*
     download data from server
     */
    
    [self loadData:URLString success:success failure:failure];
}

#pragma mark - Private Interface

-(void)loadData:(NSString*)URLString success:(void_block_id)success failure:(void_block_string)failure
{
    /*
     try to download data from server
     */
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //add custom headers for distinguish our request on backend
    //from another mobile client request(android for example or another version of client)
    [[YCApplicationData customHeaders] bk_each:^void(id key, id value) {
        [manager.requestSerializer setValue:value forHTTPHeaderField:key];
    }];
    
    //debug request
    if (DEBUG_NETWORK_REQUEST) {
        DLog(@"url=%@", URLString);
        DLog(@"params=%@", [self params]);
    }
    
    [manager GET:URLString parameters:[self params] success:^(AFHTTPRequestOperation *operation, id responseObject) {

        //debug response
        if (DEBUG_NETWORK_RESPONSE) {
            DLog(@"response=%@", responseObject);
        }
        
        //pass data to upper engine
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //notify upper engine about download failure
        failure([NSString stringWithFormat:@"Error: %@", error]);
    }];
}

#pragma mark - params for network request
- (NSDictionary*)params
{
    //by default - no params
    return @{};
}

@end
