//
//  BaseEngine.h
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YCBlocks.h"

@interface YCBaseEngine : NSObject

-(void)fetch:(NSString*)url
     success:(void_block_id)success
     failure:(void_block_string)failure;

@end
          