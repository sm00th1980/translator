//
//  YCApplicationData.h
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCApplicationData : NSObject

+ (NSString*)applicationName;
+ (NSString*)applicationVersion;

+ (NSDictionary*)customHeaders;

@end
