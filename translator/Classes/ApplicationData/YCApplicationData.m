//
//  YCApplicationData.m
//  translator
//
//  Created by Deyarov Ruslan on 14.11.14.
//  Copyright (c) 2014 Deyarov Ruslan. All rights reserved.
//

#import "YCApplicationData.h"

@implementation YCApplicationData

#pragma mark - publice interface
+ (NSString*)applicationVersion
{
    //application version
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [NSString stringWithFormat:@"%@", [infoDictionary objectForKey:@"CFBundleShortVersionString"]];
}

+ (NSString*)applicationName
{
    //application name
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [NSString stringWithFormat:@"%@", [infoDictionary objectForKey:@"CFBundleDisplayName"]];
}

+ (NSDictionary*)customHeaders
{
    return @{
             @"x-client-identifier" : @"iOS",
             @"x-client-appname" : [self applicationName],
             @"x-client-version" : [self applicationVersion]
             };
}

@end
